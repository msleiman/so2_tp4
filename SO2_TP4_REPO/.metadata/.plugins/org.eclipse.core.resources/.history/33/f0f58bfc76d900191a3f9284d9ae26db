#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MK64F12.h"
#include "fsl_debug_console.h"
#include "FreeRTOS.h"
#include "task.h"
#include "math.h"
#include "queue.h"




#include <TP4_FTOS.h> //include mi propio header


struct AMessage
{
	int ucMessageID;
    void *pointer;
} ;


/*
 * @brief   Application entry point.
 */




int main(void) {

  	/* Init board hardware. */

    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitBootPeripherals();


  	/* Init FSL debug console. */
    BOARD_InitDebugConsole();

    //PRINTF("Start \r\n");


    //Enable VTRACE
    vTraceEnable(TRC_START);

    /* Create a queue capable of containing 10 pointers to AMessage
    structures.  These are to be queued by pointers as they are
    relatively large structures. */
    QueueHandle_t xQueue;
    xQueue = xQueueCreate( 10, sizeof( struct AMessage)  );


    /*Iniciamos el ADC*/
    Init_ADC();


    if( xQueue == NULL )
    {
		PRINTF("ERORR: Problema al crear la cola! \r\n");
		exit(1); //problema para crear la cola
    }

    //xTaskCreate( producerTASK, ( signed char * ) "producerTASK", 512, (void *)xQueue, PRIORITY_PRODUCER, ( xTaskHandle * ) NULL );

    xTaskCreate( consumerTASK, ( signed char * ) "consumerTASK", 512, (void *)xQueue, PRIORITY_CONSUMER, ( xTaskHandle * ) NULL );

    xTaskCreate( keyboardTASK, ( signed char * ) "keyboardTASK", 512, (void *)xQueue, PRIORITY_KEYBOARD, ( xTaskHandle * ) NULL );// //defimos un stack de 5|2 (debido al buffer)

    xTaskCreate(   sensorTASK, ( signed char * )   "sensorTASK", 512, (void *)xQueue, PRIORITY_SENSOR, ( xTaskHandle * ) NULL );



    vTaskStartScheduler();

   //should never get here
   PRINTF("ERORR: vTaskStartScheduler returned! \r\n");
   for (;;);

}


/*Funcion que inicia el ADC, triggered por software y con resolucion de 8 bits*/
void Init_ADC(){

	SIM->SCGC6 |= SIM_SCGC6_ADC0_MASK; //Enciende el clock de ADC0
	SIM->SOPT7=SIM_SOPT7_ADC0PRETRGSEL(0);//activacion por trigger
	ADC0->SC1[0]=ADC_SC1_DIFF(0); //Single-ended
	ADC0->CFG1=ADC_CFG1_ADIV(0); //divisor de 1, queda en 8MHz de frecuencia
	ADC0->CFG1=ADC_CFG1_MODE(0); // 8 bits de resolucion de conversion
	ADC0->CFG1=ADC_CFG1_ADICLK(0); //seleccion como fuente reloj interno el del bus
	ADC0->CFG1=ADC_CFG1_ADLPC(0); //normal mode (no se configura en baja potencia)
	ADC0->CFG2=ADC_CFG2_ADHSC(0); //conversion a velocidad normal
	ADC0->SC2=ADC_SC2_DMAEN(0); //DMA desactivado
	ADC0->SC2=ADC_SC2_ADTRG(0); //Software Trigger Select (activacion de la conversion por software)
	ADC0->SC3=ADC_SC3_AVGE(1); //Hardware Average Enable
	ADC0->SC3=ADC_SC3_AVGS(3); //16 samples averaged

}

/*Es el programa prodcutor que obtiene la conversion del ADC, proveniente del sensor de
 * "temperatura". Este valor tiene una longitud fija de 8 bites (1 byte).
 * Esta tarea es periodica y su periodo es definido por la variable DELAY_SENSOR_MS.
 * Cuando la conversion finaliza el resultado es guardado en un mensaje con un ID
 * especifico para la tarea y el resultado de la conversion. El mensaje luego es almacenado en
 * la cola, que comparten los productores y el consumidor.
 */

void sensorTASK( void * pvParameters ){

	/*tomo la cola que viene por parametro*/
    QueueHandle_t xQueue;
    xQueue = (QueueHandle_t) pvParameters;

    /*creo un struc mensaje para almacenar la lectura*/
    struct AMessage pxMessage;


	/*Agrego el ID para el consumidor*/
	pxMessage.ucMessageID = ID_SENSOR;

    /*creo una variable para almacenar el resultado de la conversion*/
	uint8_t result_sensor;


    for (;;) {


    	/*Lectura del ADC*/
    	ADC0->SC1[0]=ADC_SC1_ADCH(0); //Selecciona el canal 0 (pin ADC0_DP0)
    	while(ADC0->SC2 & ADC_SC2_ADACT_MASK);
    	while(!(ADC0->SC1[0] &ADC_SC1_COCO_MASK));
    	result_sensor = ADC0->R[0];


    	/*almaceno el resultado de la conversion en el mensaje*/
    	pxMessage.pointer = result_sensor;


		if( xQueue != 0 )
			{
				//Send a pointer to a struct AMessage object.  DWait for 10 ticks for space to become
				//available if necessary.

				if(  xQueueSendToBack( xQueue, (void *) &pxMessage, ( TickType_t ) 10) != pdPASS ){
					 // Failed to post the message, even after 10 ticks.
					  PRINTF("SENSOR: No puede escribir el mensaje en la cola. \r\n");
				}else{
					  PRINTF("SENSOR: Mensaje enviado -> ID: %d - Contenido: %i. \r\n", pxMessage.ucMessageID, pxMessage.pointer);
				}
			}

		vTaskDelay(DELAY_SENSOR_MS/portTICK_RATE_MS);

    }
}



/*Esta tarea represeta un buffer con entradas de usuario por teclado.
 * Se crea un buffer de TAM_BUFF_KEYBOARD palabras (Filas) con logitud maxima de palabra
 * de TAM_WORD_KEYBOARD (columnas).
 * A medida que se van generando entradas de usuario (de longitud aleatoria y caracteres aleatorios)
 * y de forma aperiodica (periodos aleatorios), se va almacenando cada una de estas palabras en el
 * mensaje y el mensaje es enviado a la cola. El mensaje se pasa por referencia por lo tanto la cola
 * obtendra la posicion de memoria de la palabra al momento de la lectura. A medidad que se producen las
 * entradas de usuario se van usando las palabras del buffer de forma circular.
 */
void keyboardTASK( void * pvParameters )
{


	/*tomo la cola que viene por parametro*/
    QueueHandle_t xQueue;
    xQueue = (QueueHandle_t) pvParameters;

    /*creo un struc mensaje para almacenar la lectura*/
    struct AMessage pxMessage;

    pxMessage.ucMessageID = ID_KEYBOARD; //defino ID del mensaje (Tipo 2)

    int i; //variables para bucles
    int random_longitud;  //longitud random varia entre 1 y el tamaño buffer
    TickType_t randomDelay; //delay aleatoria de la tarea

    //defino un buffer para las entradas de usuario
    char buffer[TAM_BUFF_KEYBOARD][TAM_WORD_KEYBOARD];

    //variable para recorrer el buffer
    int pos = 0;


    for (;;) {


    	//definimos un buffer y se lo asignamos al mensaj (ID ya asginado)

    	pxMessage.pointer = &buffer[pos];


    	//PRINTF("buffer: %i. \r\n", &buffer[pos]);
    	//PRINTF("pointer: %i. \r\n", pxMessage.pointer );

    	//generamos una longitud aleatoria que va entre 1 y TAM_BUFF_KEYBOARD
    	random_longitud=(rand()%(TAM_WORD_KEYBOARD-1))+1;
    	//PRINTF("longitd random %d. \r\n", random_longitud);


    	//limpieza del buffer
    	for(i=0; i<TAM_WORD_KEYBOARD; i++){
    		buffer[pos][i]='\0';
    	}

    	//cargamos con una cadena aleatoria
    	for(i=0; i<random_longitud; i++){
    		buffer[pos][i]= 'A' + (rand() % 26); //generamos una letra aleatoria del telcado
    	}


		if( xQueue != 0 )
			{
				//Send a pointer to a struct AMessage object.  DWait for 10 ticks for space to become
				//available if necessary.

				if(  xQueueSendToBack( xQueue, (void *) &pxMessage, ( TickType_t ) 10) != pdPASS ){
					 // Failed to post the message, even after 10 ticks.
						PRINTF("No puede escribir el mensaje en la cola! \r\n");
				}else{
				     	PRINTF("TECLADO: Mensaje enviado -> ID: %d - Contenido: %s. \r\n", pxMessage.ucMessageID, pxMessage.pointer);

				}
			}

		pos = (pos + 1) % TAM_BUFF_KEYBOARD;

    	//generamos aleatorio un valor entre 0 y MAX_ALE_KEYBOARD_MS de retardo
    	randomDelay=rand()%DELAY_KEYBOARD_MS;
    	//PRINTF("delay random %d. \r\n", randomDelay);

		vTaskDelay(randomDelay/portTICK_RATE_MS);

    }

}



/*Aplicacion consumidora que se encarga de verificar que en la cola existan mensajes disponibles.
 * Y en caso de estar disponibles envia estos mensajes traves de modulo UART.
 */
void consumerTASK( void * pvParameters )
{

	/*tomo la cola que viene por parametro*/
    QueueHandle_t xQueue;
    xQueue = (QueueHandle_t) pvParameters;

    /*creo un struc mensaje para almacenar la lectura*/
    struct AMessage pxRxedMessage;

    UBaseType_t waitingQueue; //para almacenar numero de mensajes esperando en la cola

    for (;;) {



        if( xQueue != 0 )
        {
        	waitingQueue =  uxQueueMessagesWaiting(  xQueue );
			PRINTF ("numero de mensajes esperando %d. \r\n", waitingQueue);

            if( waitingQueue > 0 )
            {


            // Receive a message on the created queue.  Block for 10 ticks if a
            // message is not immediately available.

            	for (int i = 0; i < waitingQueue; i++) {



					if( xQueueReceive( xQueue, &pxRxedMessage, ( TickType_t ) 10) ) //== pdPASS )
					{
						// pcRxedMessage now points to the struct AMessage variable


						switch ( pxRxedMessage.ucMessageID )
						{

							case ID_PRODUCER:
								PRINTF("CONSUMIDOR: Mensaje recibido del productor-> ID: %d - Contenido: %d. \r\n", pxRxedMessage.ucMessageID, pxRxedMessage.pointer);
								break;
							case ID_KEYBOARD:
								PRINTF("CONSUMIDOR: Mensaje recibido del teclado-> ID: %d - Contenido: %s.\r\n", pxRxedMessage.ucMessageID, pxRxedMessage.pointer);
								break;
							case ID_SENSOR:
								PRINTF("CONSUMIDOR: Mensaje recibido del sensor-> ID: %d - Contenido: %i.\r\n", pxRxedMessage.ucMessageID, pxRxedMessage.pointer);
								break;
							default:
								break;
						}


					} else {
						PRINTF("No pude recibir el mensaje! \r\n");

					}
            	}

               }

        }

        vTaskDelay(DELAY_CONSUMER_MS/portTICK_RATE_MS);

    }

}


void producerTASK( void * pvParameters )
{


	/*tomo la cola que viene por parametro*/
    QueueHandle_t xQueue;
    xQueue = (QueueHandle_t) pvParameters;

    /*creo un struc mensaje para almacenar la lectura*/
    struct AMessage pxMessage;

    pxMessage.ucMessageID = ID_PRODUCER;

    int i = 0;

    for (;;) {


        pxMessage.pointer = i;


		if( xQueue != 0 )
			{
				//Send a pointer to a struct AMessage object.  DWait for 10 ticks for space to become
				//available if necessary.

				if(  xQueueSendToBack( xQueue, (void *) &pxMessage, ( TickType_t ) 10) != pdPASS ){
					 // Failed to post the message, even after 10 ticks.
					 	PRINTF("No puede escribir el mensaje en la cola. \r\n");
				}else{
					PRINTF("PRODUCTOR: Mensaje enviado -> ID: %d - Contenido: %d. \r\n", pxMessage.ucMessageID, pxMessage.pointer);

				}
			}

		i = i + 1;

		vTaskDelay(DELAY_PRODUCER/portTICK_RATE_MS);

    }

}
