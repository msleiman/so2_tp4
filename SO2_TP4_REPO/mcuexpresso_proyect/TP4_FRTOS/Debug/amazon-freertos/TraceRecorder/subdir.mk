################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../amazon-freertos/TraceRecorder/trcKernelPort.c \
../amazon-freertos/TraceRecorder/trcSnapshotRecorder.c \
../amazon-freertos/TraceRecorder/trcStreamingRecorder.c 

OBJS += \
./amazon-freertos/TraceRecorder/trcKernelPort.o \
./amazon-freertos/TraceRecorder/trcSnapshotRecorder.o \
./amazon-freertos/TraceRecorder/trcStreamingRecorder.o 

C_DEPS += \
./amazon-freertos/TraceRecorder/trcKernelPort.d \
./amazon-freertos/TraceRecorder/trcSnapshotRecorder.d \
./amazon-freertos/TraceRecorder/trcStreamingRecorder.d 


# Each subdirectory must supply rules for building sources it contributes
amazon-freertos/TraceRecorder/%.o: ../amazon-freertos/TraceRecorder/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DCPU_MK64FN1M0VLL12 -DCPU_MK64FN1M0VLL12_cm4 -DFSL_RTOS_BM -DSDK_OS_BAREMETAL -DCR_INTEGER_PRINTF -DPRINTF_FLOAT_ENABLE=0 -DFSL_RTOS_FREE_RTOS -DSDK_OS_FREE_RTOS -D__MCUXPRESSO -D__USE_CMSIS -DDEBUG -DSDK_DEBUGCONSOLE=1 -I../drivers -I../component/lists -I../source -I../utilities -I../component/uart -I../component/serial_manager -I../device -I../amazon-freertos/freertos/portable -I../amazon-freertos/include -I../CMSIS -I../board -I../ -I../board/boards -O0 -fno-common -g3 -Wall -c -ffunction-sections -fdata-sections -ffreestanding -fno-builtin -mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


